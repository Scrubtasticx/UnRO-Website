<?php
if (!defined('FLUX_ROOT')) exit;

$title = 'Guild Ownage Ranking';
$bind     = array();

$col  = "g.guild_id, g.char_id, g.master AS guild_master, g.name, g.guild_lv, g.emblem_len, ";
$col .= "ownladder.currentown AS ol_currentown, ownladder.highestown AS ol_highestown ";

$sql  = "SELECT $col FROM {$server->charMapDatabase}.guild AS g ";
$sql .= "LEFT JOIN {$server->charMapDatabase}.ownladder ON ownladder.guild_id = g.guild_id ";
$sql .= "LEFT JOIN {$server->charMapDatabase}.char ON char.char_id = g.char_id ";
$sql .= "LEFT JOIN {$server->loginDatabase}.login ON login.account_id = char.account_id ";

$groups = AccountLevel::getGroupID((int)Flux::config('RankingHideGroupLevel'), '<');
if(!empty($groups)) {
	$ids   = implode(', ', array_fill(0, count($groups), '?'));
	$sql  .= "WHERE login.group_id IN ($ids) ";
	$bind  = array_merge($bind, $groups);
}

$sql .= "ORDER BY ownladder.highestown DESC ";
$sql .= "LIMIT ".(int)Flux::config('GuildOwnageRankingLimit');
$sth  = $server->connection->getStatement($sql);

$sth->execute($bind);
$guilds = $sth->fetchAll();
?>