<?php
return array(

	// If view isn't found in this template it
	// will load the files from the inherited template
	// So basically, if you want to create a new theme, you don't
	// need to put all the files here again, just the modified views are enough.
	'inherit'     => 'default',

	// This data is not displayed anywhere.
	'author'      => 'iAmGnome',
	'version'     => '1.1',
	'description' => 'Responsive Theme for FluxCP',
	'website'     => 'https://rathena.org/board/profile/44371-iamgnome/'
);
?>
