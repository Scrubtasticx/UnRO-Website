<?php if (!defined('FLUX_ROOT')) exit; ?>
<h2>Guild Ownage Ranking</h2>
<h3>
	Top <?php echo number_format($limit=(int)Flux::config('GuildOwnageRankingLimit')) ?> Guilds Ownage
	on <?php echo htmlspecialchars($server->serverName) ?>
</h3>
<?php if ($guilds): ?>
<div class="adjust">
	<table id="restable" class="table table-striped">
		<tr id="hrow">
			<th>Rank</th>
			<th colspan="2">Guild</th>
			<th>Guild Leader</th>
			<th>Highest Ownage</th>
			<th>Current Ownage</th>
		</tr>
		<?php for ($i = 0; $i < $limit; ++$i): ?>
		<tr<?php if (!isset($guilds[$i])) echo ' class="empty-row"'; if ($i === 0) echo ' class="top-ranked" title="<strong>'.htmlspecialchars($guilds[$i]->name).'</strong> is the top ranked guild in terms of ownage!"' ?>>
			<td align="left" tableHeadData='Rank'><?php echo number_format($i + 1) ?></td>
			<?php if (isset($guilds[$i])): ?>
			<td colspan="2" tableHeadData='Guild'<?php if (!$guilds[$i]->emblem_len)?>>
				<?php if ($auth->actionAllowed('guild', 'view') && $auth->allowedToViewGuild): ?>
					<img src="<?php echo $this->emblem($guilds[$i]->guild_id) ?>" />  <?php echo $this->linkToGuild($guilds[$i]->guild_id, $guilds[$i]->name) ?>
				<?php else: ?>
					<img src="<?php echo $this->emblem($guilds[$i]->guild_id) ?>" />  <?php echo htmlspecialchars($guilds[$i]->name) ?>
				<?php endif ?>
			</td>
			<td tableHeadData='Gyukd Keader'><?php echo htmlspecialchars($guilds[$i]->guild_master) ?></td>
			<td tableHeadData='Highest Ownage'><?php echo number_format($guilds[$i]->ol_highestown) ?></td>
			<td tableHeadData='Current Ownage'><?php echo number_format($guilds[$i]->ol_currentown) ?></td>
			<?php else: ?>
			<td colspan="8" tableHeadData='None'></td>
			<?php endif ?>
		</tr>
		<?php endfor ?>
	</table>
</div>
<?php else: ?>
<p>No guilds found. <a href="javascript:history.go(-1)">Go back</a>.</p>
<?php endif ?>