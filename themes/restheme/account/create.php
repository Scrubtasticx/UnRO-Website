<?php if (!defined('FLUX_ROOT')) exit; ?>
<h2><?php echo htmlspecialchars(Flux::message('AccountCreateHeading')) ?></h2>
<?php if (isset($errorMessage)): ?>
<p class="red" style="font-weight: bold"><?php echo htmlspecialchars($errorMessage) ?></p>
<?php endif ?>
<div class="col-sm-6">
	<form action="<?php echo $this->url ?>" method="post" class="form-horizontal">
		<?php if (count($serverNames) === 1): ?>
		<input type="hidden" name="server" value="<?php echo htmlspecialchars($session->loginAthenaGroup->serverName) ?>">
		<?php endif ?>
		<table class="table">
			<?php if (count($serverNames) > 1): ?>
			<tr>
				<th><label for="register_server"><?php echo htmlspecialchars(Flux::message('AccountServerLabel')) ?></label></th>
				<td>
					<select name="server" id="register_server"<?php if (count($serverNames) === 1) echo ' disabled="disabled"' ?>>
					<?php foreach ($serverNames as $serverName): ?>
						<option value="<?php echo htmlspecialchars($serverName) ?>"<?php if ($params->get('server') == $serverName) echo ' selected="selected"' ?>><?php echo htmlspecialchars($serverName) ?></option>
					<?php endforeach ?>
					</select>
				</td>
			</tr>
			<?php endif ?>
			<div class='col-sm-12'>
				<div class="form-g inner-addon left-addon">
			      <i class="fa fa-user">&nbsp;</i>
			      <input type="text" class="form-control" name="username"  id="register_username" placeholder="Username" value="<?php echo htmlspecialchars($params->get('username')) ?>"	 />
			    </div>
			</div>
			<div class='col-sm-12'>
			    <div class="form-g inner-addon left-addon">
			      <i class="fa fa-unlock-alt">&nbsp;</i>
			      <input class="form-control" type="password" name="password" id="register_password" placeholder="Password" />
			    </div>
			</div>
			<div class='col-sm-12'>
			    <div class="form-g inner-addon left-addon">
			      <i class="fa fa-lock">&nbsp;</i>
			      <input class="form-control" type="password" name="confirm_password" id="register_confirm_password" placeholder="Confirm Password" />
			    </div>
			</div>
			<div class='col-sm-12'>
			     <div class="form-g inner-addon left-addon">
			      <i class="fa fa-envelope">&nbsp;</i>
			      <input class="form-control" type="email" name="email_address" id="register_email_address" placeholder="Email Address" value="<?php echo htmlspecialchars($params->get('email_address')) ?>" />
			    </div>
			</div>
			<div class='col-sm-12'>
			     <div class="form-g inner-addon left-addon">
			      <i class="fa fa-envelope">&nbsp;</i>
			      <input class="form-control" type="email2" name="email_address2" id="register_email_address2" placeholder="Confirm Email Address" value="<?php echo htmlspecialchars($params->get('email_address2')) ?>" />
			    </div>
			</div>
			<div class='col-sm-12'>
				<div class="icon-label">
					<i class="fa fa-venus-mars">&nbsp;</i>
					<label>Gender</label>
					<i class="fa fa-question" title="The gender you choose here will affect your in-game character's gender!">&nbsp;</i>
				</div>
			    <div class="radio-form-g">

						<input type="radio" name="gender" id="register_gender_m" value="M"/>
						<label for="register_gender_m">Male</label>
						<div class="check"></div>
				</div>
				<div class="radio-form-g">
						<input type="radio" name="gender" id="register_gender_f" value="F"/>
						<label for="register_gender_f">Female</label>
						<div class="check">
							<div class="inside">
							</div>
							</div>
				</div>
			</div>
			<div class='col-sm-12'>
				<div class="form-g left-addon">
					<div class="icon-label">
							<i class="fa fa-calendar">&nbsp;</i>
								<label>Birthdate</label>
					</div>
					<span class="date-field">
						<select name="birthdate_year">
							<option value="" selected disabled>Year</option>
							<option value="2017" >2017</option>
							<option value="2016">2016</option>
							<option value="2015">2015</option>
							<option value="2014">2014</option>
							<option value="2013">2013</option>
							<option value="2012">2012</option>
							<option value="2011">2011</option>
							<option value="2010">2010</option>
							<option value="2009">2009</option>
							<option value="2008">2008</option>
							<option value="2007">2007</option>
							<option value="2006">2006</option>
							<option value="2005">2005</option>
							<option value="2004">2004</option>
							<option value="2003">2003</option>
							<option value="2002">2002</option>
							<option value="2001">2001</option>
							<option value="2000">2000</option>
							<option value="1999">1999</option>
							<option value="1998">1998</option>
							<option value="1997">1997</option>
							<option value="1996">1996</option>
							<option value="1995">1995</option>
							<option value="1994">1994</option>
							<option value="1993">1993</option>
							<option value="1992">1992</option>
							<option value="1991">1991</option>
							<option value="1990">1990</option>
							<option value="1989">1989</option>
							<option value="1988">1988</option>
							<option value="1987">1987</option>
						</select>

						<select name="birthdate_month">
							<option value="" selected disabled>Month</option>
							<option value="01">01</option>
							<option value="02">02</option>
							<option value="03">03</option>
							<option value="04">04</option>
							<option value="05">05</option>
							<option value="06">06</option>
							<option value="07">07</option>
							<option value="08">08</option>
							<option value="09">09</option>
							<option value="10">10</option>
							<option value="11">11</option>
							<option value="12">12</option>
						</select>

						<select name="birthdate_day">
							<option value="" selected disabled>Day</option>
							<option value="01">01</option>
							<option value="02">02</option>
							<option value="03">03</option>
							<option value="04">04</option>
							<option value="05">05</option>
							<option value="06">06</option>
							<option value="07">07</option>
							<option value="08">08</option>
							<option value="09">09</option>
							<option value="11">11</option>
							<option value="16">16</option>
							<option value="17">17</option>
							<option value="18">18</option>
							<option value="19">19</option>
							<option value="20">20</option>
							<option value="21">21</option>
							<option value="22">22</option>
							<option value="23">23</option>
							<option value="24">24</option>
							<option value="25">25</option>
							<option value="26">26</option>
							<option value="27">27</option>
							<option value="28">28</option>
							<option value="29">29</option>
							<option value="30">30</option>
							<option value="31">31</option>
						</select>
					</span>
				</div>
			</div>
			<?php if (Flux::config('UseCaptcha')): ?>
			<div class='col-sm-12'>
				<div class="form-g left-addon">
					<?php if (Flux::config('EnableReCaptcha')): ?>
						<label for="register_security_code"><?php echo htmlspecialchars(Flux::message('AccountSecurityLabel')) ?></label>
						<div class="g-recaptcha" data-theme = "<?php echo $theme;?>" data-sitekey="<?php echo $recaptcha ?>">
					<?php else: ?>
					<div class="icon-label">
						<label for="register_security_code"><?php echo htmlspecialchars(Flux::message('AccountSecurityLabel')) ?></label>
					</div>
					<div>
							<div class="security-code">
									<img src="<?php echo $this->url('captcha') ?>" />
							</div>
								<input type="text" name="security_code" id="register_security_code" />
							<div style="font-size: smaller;" class="action">
								<strong><a href="javascript:refreshSecurityCode('.security-code img')"><?php echo htmlspecialchars(Flux::message('RefreshSecurityCode')) ?></a></strong>
							</div>
					</div>
					<?php endif ?>
				</div>
			</div>
			<?php endif ?>
			<div class='col-sm-12'>
				<div style="margin-bottom: 5px">
						<?php printf(htmlspecialchars(Flux::message('AccountCreateInfo2')), '<a href="'.$this->url('service', 'tos').'">'.Flux::message('AccountCreateTerms').'</a>') ?>
					<div>
						<button class="form-btn" type="submit"><strong><?php echo htmlspecialchars(Flux::message('AccountCreateButton')) ?></strong></button>
					</div>
				</div>
			</div>
		</table>
	</form>
</div>
<div class="col-sm-6">
	<h3><strong>Registration Notice</strong></h3>
	<p><?php printf(htmlspecialchars(Flux::message('AccountCreateInfo')), '<a href="'.$this->url('service', 'tos').'">'.Flux::message('AccountCreateTerms').'</a>') ?></p>
	<?php if (Flux::config('RequireEmailConfirm')): ?>
	<p><strong>Note:</strong> You will need to provide a working e-mail address to confirm your account before you can log-in.</p>
	<?php endif ?>
	<p><strong>Note:</strong> <?php echo sprintf("Your password must be between %d and %d characters.", Flux::config('MinPasswordLength'), Flux::config('MaxPasswordLength')) ?></p>
	<?php if (Flux::config('PasswordMinUpper') > 0): ?>
	<p><strong>Note:</strong> <?php echo sprintf(Flux::message('PasswordNeedUpper'), Flux::config('PasswordMinUpper')) ?></p>
	<?php endif ?>
	<?php if (Flux::config('PasswordMinLower') > 0): ?>
	<p><strong>Note:</strong> <?php echo sprintf(Flux::message('PasswordNeedLower'), Flux::config('PasswordMinLower')) ?></p>
	<?php endif ?>
	<?php if (Flux::config('PasswordMinNumber') > 0): ?>
	<p><strong>Note:</strong> <?php echo sprintf(Flux::message('PasswordNeedNumber'), Flux::config('PasswordMinNumber')) ?></p>
	<?php endif ?>
	<?php if (Flux::config('PasswordMinSymbol') > 0): ?>
	<p><strong>Note:</strong> <?php echo sprintf(Flux::message('PasswordNeedSymbol'), Flux::config('PasswordMinSymbol')) ?></p>
	<?php endif ?>
	<?php if (!Flux::config('AllowUserInPassword')): ?>
	<p><strong>Note:</strong> <?php echo Flux::message('PasswordContainsUser') ?></p>
	<?php endif ?>
</div>
