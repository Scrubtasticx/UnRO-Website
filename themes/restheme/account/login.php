<?php if (!defined('FLUX_ROOT')) exit; ?>
<h2><?php echo htmlspecialchars(Flux::message('LoginHeading')) ?></h2>
<?php if (isset($errorMessage)): ?>
<p class="red"><?php echo htmlspecialchars($errorMessage) ?></p>
<?php else: ?>

<?php if ($auth->actionAllowed('account', 'create')): ?>
<p><?php printf(Flux::message('LoginPageMakeAccount'), $this->url('account', 'create')); ?></p>
<p>Creating your account grants full usage of the website's account panel.</p>
<?php endif ?>

<?php endif ?>
<form action="<?php echo $this->url('account', 'login', array('return_url' => $params->get('return_url'))) ?>" method="post" class="generic-form">
	<?php if (count($serverNames) === 1): ?>
	<input type="hidden" name="server" value="<?php echo htmlspecialchars($session->loginAthenaGroup->serverName) ?>">
	<?php endif ?>
	<table class="table">
		<div class='col-md-7'> 
			<div class="form-g inner-addon left-addon">
				<i class="fa fa-user">&nbsp;</i>   
				<input type="text" class="form-control" name="username" id="login_username" placeholder="Username" value="<?php echo htmlspecialchars($params->get('username')) ?>"	/>
				   
			</div>
		</div>
		<div class='col-md-7'> 
			<div class="form-g inner-addon left-addon">
				<i class="fa fa-lock">&nbsp;</i>      
				<input class="form-control"  type="password" name="password" id="login_password" placeholder="Password" />
			</div>
		</div>	
		<div class='col-md-7'> 
			<p align="right"><a href="<?php echo $this->url('account','resetpass'); ?>">Forgot Password</a> </p>
		</div>
		
		<div class='col-md-7'> 
			<div>
				<input  class="form-btn" type="submit" value="<?php echo htmlspecialchars(Flux::message('LoginButton')) ?>" />
			</div>
		</div>
		<?php if (Flux::config('UseCaptcha')): ?>
		<div class='col-sm-12'>
			<div class="form-g left-addon">
				<?php if (Flux::config('EnableReCaptcha')): ?>
				<div class="icon-label">
					<label for="register_security_code"><?php echo htmlspecialchars(Flux::message('AccountSecurityLabel')) ?></label>
				</div>
				<div>
						<div class="security-code">
								<img src="<?php echo $this->url('captcha') ?>" />
						</div>
							<input type="text" name="security_code" id="register_security_code" />
						<div style="font-size: smaller;" class="action">
							<strong><a href="javascript:refreshSecurityCode('.security-code img')"><?php echo htmlspecialchars(Flux::message('RefreshSecurityCode')) ?></a></strong>
						</div>

				<?php else: ?>
				</div>
				<?php endif ?>
			</div>
		</div>
		<?php endif ?>
	</table>
</form>
