<?php if (!defined('FLUX_ROOT')) exit; ?>
<div class="col-md-12">	
	<h2><?php echo htmlspecialchars(Flux::message('PasswordChangeHeading')) ?></h2>
	<?php if (!empty($errorMessage)): ?>
		<p class="red"><?php echo htmlspecialchars($errorMessage) ?></p>
	<?php else: ?>
	<div class='info'>
		<p><?php echo htmlspecialchars(Flux::message('PasswordChangeInfo')) ?></p>
	</div>
	<?php endif ?>
	<br />
	<form action="<?php echo $this->urlWithQs ?>" method="post" class="generic-form">
		<table class="table">
			<div class="col-md-7">
				<div class="form-g inner-addon left-addon">
					<i class="fa fa-unlock-alt">&nbsp;</i>
					<input class="form-control" type="password" name="currentpass" id="currentpass" placeholder="Current Password" />
				</div>
			</div>
			<div class="col-md-7" >
				<div class="form-g inner-addon left-addon">
					<i class="fa fa-unlock-alt">&nbsp;</i>
					<input class="form-control" type="password" name="newpass" id="newpass" placeholder="New Password" />
				</div>
			</div>
			<div class="col-md-7" >
				<div class="form-g inner-addon left-addon">
					<i class="fa fa-lock">&nbsp;</i>
					<input class="form-control" type="password" name="confirmnewpass" id="confirmnewpass" placeholder="Confirm New Password" />
				</div>
			</div>
			<div class="col-md-7" >
				<input class="form-btn" type="submit" value="<?php echo htmlspecialchars(Flux::message('PasswordChangeButton')) ?>" />
			</div>
			</div>
				<p class="important"><strong>Note:</strong></p>
				<p><?php echo htmlspecialchars(Flux::message('PasswordChangeNote')) ?></p>
				<p><?php echo htmlspecialchars(Flux::message('PasswordChangeNote2')) ?></p>
		</table>
	</form>
</div>
