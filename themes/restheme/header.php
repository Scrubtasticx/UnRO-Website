<?php if (!defined('FLUX_ROOT')) exit; ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<?php if (isset($metaRefresh)): ?>
		<meta http-equiv="refresh" content="<?php echo $metaRefresh['seconds'] ?>; URL=<?php echo $metaRefresh['location'] ?>" />
		<?php endif ?>
		<meta property="og:type" content="website" />
		<meta property="og:site_name" content="UnRO" />
		<meta property="og:url" content="INSERT URL HERE" />
		<meta property="og:title" content="UnRO" />
		<meta property="og:image" content="INSERT IMAGE LINK HERE" />
		<meta property="og:image:type" content="image/png" />
		<meta property="og:image:width" content="400" />
		<meta property="og:image:height" content="200" />
		<meta property="og:description" content="YOUR DESCRIPTION" />
		<link rel="icon" type="img" href="favicon.ico" sizes="48x48">
		<title>UnRO</title>
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
		<!--<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">-->
		<link rel="stylesheet" href="<?php echo $this->themePath('css/fontawesome-all.css') ?>" type="text/css" media="screen" title="" charset="utf-8" />
		<link rel="stylesheet" href="<?php echo $this->themePath('css/flux.css') ?>" type="text/css" media="screen" title="" charset="utf-8" />
		<link rel="stylesheet" href="<?php echo $this->themePath('css/style.css') ?>" type="text/css" media="screen" title="" charset="utf-8" />
		<link rel="stylesheet" href="<?php echo $this->themePath('css/animate.css') ?>" type="text/css" media="screen" title="" charset="utf-8" />
		<link rel="stylesheet" href="<?php echo $this->themePath('css/waypoints.css') ?>" type="text/css" media="screen" title="" charset="utf-8" />
		<link rel="stylesheet" href="<?php echo $this->themePath('css/feed-timeline.css') ?>" type="text/css" media="all" />
		<link href="<?php echo $this->themePath('css/flux/unitip.css') ?>" rel="stylesheet" type="text/css" media="screen" title="" charset="utf-8" />
		<?php if (Flux::config('EnableReCaptcha')): ?>
		<link href="<?php echo $this->themePath('css/flux/recaptcha.css') ?>" rel="stylesheet" type="text/css" media="screen" title="" charset="utf-8" />
		<?php endif ?>
		<!--[if IE]>
		<link rel="stylesheet" href="<?php echo $this->themePath('css/flux/ie.css') ?>" type="text/css" media="screen" title="" charset="utf-8" />
		<![endif]-->
		<!--[if lt IE 9]>
		<script src="<?php echo $this->themePath('js/ie9.js') ?>" type="text/javascript"></script>
		<script type="text/javascript" src="<?php echo $this->themePath('js/flux.unitpngfix.js') ?>"></script>
		<![endif]-->
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
		<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
		<script type="text/javascript" src="<?php echo $this->themePath('js/jquery-1.8.3.min.js') ?>"></script>
		<script type="text/javascript" src="<?php echo $this->themePath('js/flux.datefields.js') ?>"></script>
		<script type="text/javascript" src="<?php echo $this->themePath('js/flux.unitip.js') ?>"></script>
		<script type="text/javascript" src="<?php echo $this->themePath('js/waypoints.js') ?>"></script>
		<script type="text/javascript" src="<?php echo $this->themePath('js/jquery.waypoints.min.js') ?>"></script>
		<script type="text/javascript" src="<?php echo $this->themePath('js/jquery.counterup.js') ?>"></script>
		<script type="text/javascript" src="<?php echo $this->themePath('js/jquery.counterup.min.js') ?>"></script>
		<script type="text/javascript">
			$(document).ready(function(){
				var inputs = 'input[type=text],input[type=password],input[type=file]';
				$(inputs).focus(function(){
					$(this).css({
						'background-color': '#f9f5e7',
						'border-color': '#dcd7c7',
						'color': '#726c58'
					});
				});
				$(inputs).blur(function(){
					$(this).css({
						'backgroundColor': '#ffffff',
						'borderColor': '#dddddd',
						'color': '#444444'
					}, 500);
				});
				$('.menuitem a').hover(
					function(){
						$(this).fadeTo(200, 0.85);
						$(this).css('cursor', 'pointer');
					},
					function(){
						$(this).fadeTo(150, 1.00);
						$(this).css('cursor', 'normal');
					}
				);
				$('.money-input').keyup(function() {
					var creditValue = parseInt($(this).val() / <?php echo Flux::config('CreditExchangeRate') ?>, 10);
					if (isNaN(creditValue))
						$('.credit-input').val('?');
					else
						$('.credit-input').val(creditValue);
				}).keyup();
				$('.credit-input').keyup(function() {
					var moneyValue = parseFloat($(this).val() * <?php echo Flux::config('CreditExchangeRate') ?>);
					if (isNaN(moneyValue))
						$('.money-input').val('?');
					else
						$('.money-input').val(moneyValue.toFixed(2));
				}).keyup();

				// In: js/flux.datefields.js
				processDateFields();
			});

			function reload(){
				window.location.href = '<?php echo $this->url ?>';
			}
		</script>

		<script type="text/javascript">
			function updatePreferredServer(sel){
				var preferred = sel.options[sel.selectedIndex].value;
				document.preferred_server_form.preferred_server.value = preferred;
				document.preferred_server_form.submit();
			}

			function updatePreferredTheme(sel){
				var preferred = sel.options[sel.selectedIndex].value;
				document.preferred_theme_form.preferred_theme.value = preferred;
				document.preferred_theme_form.submit();
			}

			// Preload spinner image.
			var spinner = new Image();
			spinner.src = '<?php echo $this->themePath('img/spinner.gif') ?>';

			function refreshSecurityCode(imgSelector){
				$(imgSelector).attr('src', spinner.src);

				// Load image, spinner will be active until loading is complete.
				var clean = <?php echo Flux::config('UseCleanUrls') ? 'true' : 'false' ?>;
				var image = new Image();
				image.src = "<?php echo $this->url('captcha') ?>"+(clean ? '?nocache=' : '&nocache=')+Math.random();

				$(imgSelector).attr('src', image.src);
			}
			function toggleSearchForm()
			{
				//$('.search-form').toggle();
				$('.search-form').slideToggle('fast');
			}
		</script>

		<?php if (Flux::config('EnableReCaptcha')): ?>
			<script src='https://www.google.com/recaptcha/api.js'></script>
		<?php endif ?>

	</head>
	<body>
		<?php
			$iDev = include 'main/indexconfig.php';
			$totalServers=$session->getAthenaServerNames();
		?>
		<!-- Start of Bootstrap -->
		<!-- Navigation Bar -->
		<nav class="navbar navbar-default navbar-fixed-top" role="navigation">
			<div class="container-fluid">
				<div class="navbar-header">
						<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-collapse-main">
							<span class="sr-only">Toggle Navigation</span>
							<span class="icon-bar">&nbsp;</span>
							<span class="icon-bar">&nbsp;</span>
							<span class="icon-bar">&nbsp;</span>
						</button>
						<!-- Logo -->
						<section class="os-animation" data-os-animation="rotateIn" data-os-animation-delay="0s">
							<a class="navbar-brand" href="<?php echo $this->url('main'); ?>"><img src="<?php echo $this->themePath('img/navlogo.png'); ?>"></a>
						</section>
				</div>
				<!-- Start of Navigation List -->
				<div class="collapse navbar-collapse" id="navbar-collapse-main">
					<ul class="nav navbar-nav navbar-right">
						<li><a class="active" href="<?php echo $this->url('main'); ?>">Home</a></li>
						<li><a href="forums" target="-_blank">Forums</a></li>
						<li><a target="_blank" href=" http://irowiki.org">iROwiki</a></li>
						<li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown">Shop <span class="caret">&nbsp;</span></a>
							<ul class="dropdown-menu">
								<li><a href="<?php echo $this->url('donate'); ?>">Donate</a></li>
								<li><a href="<?php echo $this->url('purchase'); ?>">Item Shop</a></li>
							</ul>
						</li>
					<?php if ($session->isLoggedIn()): ?>
						<li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown">Database <span class="caret">&nbsp;</span></a>
							<ul class="dropdown-menu">
								<li><a href="<?php echo $this->url('item'); ?>">Items</a></li>
								<li><a href="<?php echo $this->url('monster'); ?>">Monsters</a></li>
								<li><a target="_blank" href=" http://127.0.0.1/sprites/index.html">Sprites</a></li>
							</ul>
						</li>
					<?php else: ?>
						<li><a href="<?php echo $this->url('main','info'); ?>">Information</a></li>
					<?php endif;?>

					<?php if ($session->isLoggedIn()): ?>
						<li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown">Ranking <span class="caret">&nbsp;</span></a>
							<ul class="dropdown-menu">
								<li><a href="<?php echo $this->url('ranking','pvp'); ?>">PvP</a></li>
								<li><a href="<?php echo $this->url('ranking','guildownage'); ?>">Guild</a></li>
								<li><a href="<?php echo $this->url('ranking','guild'); ?>">Guild Castles</a></li>
							</ul>
						</li>
						<li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown">My Account <span class="caret">&nbsp;</span></a>
							<ul class="dropdown-menu">
								<li><a href="<?php echo $this->url('account','view'); ?>">View</a></li>
								<li><a href="<?php echo $this->url('account','logout'); ?>">Logout</a></li>
							</ul>
						</li>
					<?php else: ?>
						<li><a href="<?php echo $this->url('account','create'); ?>">Register</a></li>
						<li><a href="<?php echo $this->url('account','login'); ?>">Login</a></li>
					<?php endif;?>
					</ul>
				</div>
			</div>
		</nav>
		<!-- End of Navigation Bar -->
		<?php //if (in_array($params->get('module'), array('main'))) include $this->themePath('main/main.php', true) ?>
		<?php if($params->get('module')=='main' AND $params->get('action')=='index'): ?>
		<?php else: ?>
			<div class="contentpadding">
				<div class="container-fluid">
					<section class="os-animation" data-os-animation="bounceInUp" data-os-animation-delay=".1s">
					<?php if ($message=$session->getMessage()): ?>
						<p class="message"><?php echo htmlspecialchars($message); ?></p>
					<?php endif ?>
		<?php endif; ?>

		<?php //include $this->themePath('main/loginbox.php', true) ?>
		<?php if ($session->isLoggedIn()): ?>
		<?php include $this->themePath('main/submenu.php', true) ?>
		<?php include $this->themePath('main/pagemenu.php', true) ?>
		<?php else: ?>
		<?php endif;?>