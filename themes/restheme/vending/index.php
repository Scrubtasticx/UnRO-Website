<?php if (!defined('FLUX_ROOT')) exit; ?>
<h2>Vendors</h2>

<?php if ($vendings): ?>
    <?php echo $paginator->infoText() ?>
	<table id="restable" class="table table-striped">
        <thead>
            <tr id="hrow">
                <th><?php echo $paginator->sortableColumn('id', 'Vendor ID') ?></th>
                <th> <?php echo $paginator->sortableColumn('char_name', 'Vendor Name') ?></th>
                <th>Title</th>
                <th><?php echo $paginator->sortableColumn('map', 'Map') ?></th>
                <th>X</th>
                <th>Y</th>
                <th>Gender</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($vendings as $vending): ?>
                <tr>
                    <td width="50" align="center"  style="" tableHeadData='Vendor ID'>
                        <?php if ($auth->actionAllowed('vending', 'viewshop')): ?>
                            <a href="<?php echo $this->url('vending', 'viewshop', array("id" => $vending->id)); ?>"><?php echo $vending->id; ?></a>
                        <?php else: ?>
                            <?php echo $vending->id ?>
                        <?php endif ?>
                    </td>
                    <td style="font-weight:bold;" tableHeadData='Vendor Name'> <?php echo $vending->char_name; ?></td>
                    
                    <td tableHeadData='Title'>
                      <?php if ($auth->actionAllowed('vending', 'viewshop')): ?>
                            <a href="<?php echo $this->url('vending', 'viewshop', array("id" => $vending->id)); ?>"><?php echo $vending->title; ?></a>
                        <?php else: ?>
                            <?php echo $vending->title ?>
                        <?php endif ?>
                    </td>
                      
                    <td  style="color:blue;" tableHeadData='Map'>
                      <?php echo $vending->map ?>
                    </td>
                    
                    <td tableHeadData='X'>
                      <?php echo $vending->x ?>
                    </td>
                    
                    <td tableHeadData='Y'>
                      <?php echo $vending->y ?>
                    </td>
                    
                    <td tableHeadData='Gender'>
                      <?php echo $vending->sex ?>
                    </td>
                   
                </tr>

            <?php endforeach ?>
        </tbody>
    </table>
    <?php echo $paginator->getHTML() ?>
<?php else: ?>
    <p>No Vendors found. <a href="javascript:history.go(-1)">Go back</a>.</p>
<?php endif ?>
