<?php if (!defined('FLUX_ROOT')) exit;
	if( $iDev['enablerss'] )
		include('rsslib.php');

	$castleNames = Flux::config('CastleNames')->toArray();
	$sqlgvg	= "SELECT guild_castle.guild_id AS gcgid, guild_castle.castle_id AS cid, guild.name, guild.master, guild.guild_id AS ggid, guild.emblem_len FROM guild_castle LEFT JOIN guild ON guild_castle.guild_id = guild.guild_id WHERE guild_castle.castle_id IN (" . implode(",", $iDev['castles']) . ") ORDER BY guild_castle.castle_id";
	$sth = $server->connection->getStatement($sqlgvg);
	$sth->execute();
	$castlesOccupieds = $sth->fetchAll();

	$sqlpvp = "SELECT `pvpladder`.`kills`, `pvpladder`.`streaks`, `pvpladder`.`deaths`, `char`.`name`, `char`.`class`, `char`.`base_level`, `char`.`job_level`, `char`.`account_id`, `char`.`online`, `login`.`sex` FROM `pvpladder` LEFT JOIN `char` ON `char`.`char_id` = `pvpladder`.`char_id` LEFT JOIN `login` ON `login`.`account_id` = `char`.`account_id` WHERE `login`.`state` = '0' ORDER BY `pvpladder`.`kills` DESC, `pvpladder`.`streaks` DESC, `pvpladder`.`deaths` DESC, `char`.`base_exp` DESC LIMIT 0,5";
	$sthpvp = $server->connection->getStatement($sqlpvp);
	$sthpvp->execute();
	$pvpladder = $sthpvp->fetchAll();
?>
	<!-- Landing Page -->
	<div id="home">
		<div class="landing-text">
			<section class="os-animation" data-os-animation="bounceInUp" data-os-animation-delay="0s">
				<div class="container">
				<?php if ($message=$session->getMessage()): ?>
					<p class="message"><?php echo htmlspecialchars($message); ?></p>
				<?php endif ?>
	    		<h1>UnRO</h1>
	    		<h3>High Rate Fun Ragnarok Server!</h3>	
				<a href="<?php echo $this->url('main','download'); ?>" class="btn btn-default btn-lg">Download & Play Now!</a>
				</div>
			</section>
		</div>
	</div>
	<!-- Server Status-->
	<?php include 'status.php'; ?>
	<!-- Server News -->
	<div class="padding">
	<div class="container">
		<div class="row">
		<?php
			require("feedAPI.php");
			$feedAPI = new RSSFeed();
			$feedAPI->addFeed("http://127.0.0.1/unro/forums/index.php?forums/news-updates.3/index.rss","RNB");
			$entries = $feedAPI->getFeedEntries();
			foreach($entries['items'] as $entry) : endforeach;
		?>
		<section class="os-animation" data-os-animation="bounceInRight" data-os-animation-delay=".1s">
			<center><h2>News & Updates</h2>
			<i class="fa fa-spin fa-sync fa-2x" aria-hidden="true">&nbsp;</i></center>
			<br />
		</section>
		<section class="os-animation" data-os-animation="bounceInLeft" data-os-animation-delay=".1s">
			<div class="feed-timeline feed-timeline-vertical clearfix">
			<ul>
			<?php foreach($entries['items'] as $entry) : ?>
			<li>
				<div class="entry clearfix">
				<strong class="name"><a href="<?php echo $entry->link; ?>"><?php echo $entry->title; ?></a></strong> 
				<?php if ($entry->description)  { $description = substr($entry->description, 0, 150); } echo $description; ?>
				<br />
				<span class="date"><?php echo $entry->updated; ?></span>
				</div>
			</li>
			<?php endforeach; ?>
			</ul>
			</div>
			<br />
			<center><a href="<?php echo $this->url('news'); ?>">Read more news and updates</a></center>
		</section>
		</div>
	</div>
	</div>
	
	<!-- Showcase -->
	<div id="fixed">
		<div class="padding">
		<div class="container">
			<div class="row">
				<div class="col-12">
					<section class="os-animation" data-os-animation="bounceInDown" data-os-animation-delay=".1s">
						<div class="woe-text-form-head">WoE & PvP Ranking</div>
					</section>
				</div>
				<div class="col-xs-12 col-sm-6 col-md-4">
					<section class="os-animation" data-os-animation="zoomIn" data-os-animation-delay=".1s">
						<div class="woe-text-form">Schedule</div>
						<div class="woeSchd">
							<div class="woeEdition_1">
								<table>
									<tr>
										<td colspan="3" class="woeschd-heading" style="padding-top: 8px"><strong>First Edition WoE</strong></td>
									</tr>
									<?php foreach ($iDev['woeFE'] as $woe): $ewoeFE = explode(",", $woe); ?>
									<tr>
										<td><?php echo $ewoeFE[0]; ?></td>
										<td><div class="woe-center"><?php echo $ewoeFE[1]; ?></div></td>
										<td><div class="woe-right"><?php echo $ewoeFE[2]; ?></div></td>
									</tr>
									<?php endforeach; ?>
								</table>
							</div>
							<div class="woeEdition_2">
								<table>
									<tr>
										<td colspan="3" class="woeschd-heading"><strong>Second Edition WoE</strong></td>
									</tr>
									<?php foreach ($iDev['woeSE'] as $woe): $ewoeSE = explode(",", $woe); ?>
									<tr>
										<td><?php echo $ewoeSE[0]; ?></td>
										<td><div class="woe-center"><?php echo $ewoeSE[1]; ?></div></td>
										<td><div class="woe-right"><?php echo $ewoeFE[2]; ?></div></td>
									</tr>
									<?php endforeach; ?>
								</table>
							</div>
						</div>
					</section>
				</div>
				<div class="col-xs-12 col-sm-6 col-md-4">
					<section class="os-animation" data-os-animation="zoomIn" data-os-animation-delay=".1s">
						<div class="woe-text-form">Agit Lords</div>
						<div class="castleOwners">
							<div class="casPad">
								<div class="woeEdition_1">
									<table>
										<tr class="agitheader">
											<td>Castle</td>
											<td colspan="2">Guild</td>
										</tr>
										<tr>
											<?php foreach ($iDev['castles'] as $castleID): ?>
											<?php $found = 0; foreach( $castlesOccupieds as $g ): if ( $g->cid == $castleID ): if ($g->emblem_len): $found = 1; ?>
											<td><p class="agitfix"><?php echo "$castleNames[$castleID]"?></p></td>
											<td><img src="<?php echo $this->emblem($g->ggid) ?>"></td>
											<td><p align="right"><?php echo "$g->name"?></p></td>
											<?php endif; endif; endforeach ; ?>
											<?php if( ! $found ): ?>
												<img src="<?php echo $this->themePath('img/emblem.png'); ?>" alt="" title="Unocupied">
											<?php endif; ?>
											<?php endforeach; ?>
										</tr>							
									</table>
								</div>
							</div>
						</div>
					</section>
				</div>
				<div class="col-sm-12 col-md-4">
					<section class="os-animation" data-os-animation="zoomIn" data-os-animation-delay=".1s">
						<div class="woe-text-form">PvP Rankings</div>
						<div class="ranks">
							<div class="pvpranks">
								<table>
									<tr class="agitheader">
										<td>Name</td>
										<td style="text-align: left">Kills</td>
									</tr>
									<?php if(!empty($pvpladder)): ?>
									<?php $i=1; foreach ($pvpladder as $player): ?>
									<tr>
										<td class="name"><?php echo substr($player->name, 0, 30); ?></td>
										<td style="text-align: left" class="count"><?php echo $player->kills; ?></td>
									</tr>
									<?php endforeach; endif; ?>
								</table>
							</div>
						</div>
					</section>
				</div>
				<br>
			</div>
		</div>
		</div>
	</div>

	<!-- Server L-Info -->
	<div class="padding">
	<div class="container">
		<div class="row">
			<section class="os-animation" data-os-animation="bounceInUp" data-os-animation-delay=".1s">
				<center><h2>Server Information</h2>
				<br />
			</section>
			<div class="col-sm-6 text-center">
				<section class="os-animation" data-os-animation="bounceInLeft" data-os-animation-delay=".1s">
					<p class="lead">UnRO is a Pre server max level 500/120.</p>
					<p class="lead">Max cap for each individual stat is 450.</p>
					<p class="lead">Max cap for attack speed on UnRO is 197.</p>
					<p class="lead">Dex required for instant cast is 150.</p>
					<p class="lead">Constant PvP/WoE, and PvM events hourly.</p>
					<p class="lead">Pre server but we have all the new items.</p>
					<p class="lead">Alot of time went into balancing classes for PvP.</p>
					<p class="lead">Always updating to give you the best experience.</p>
					<p class="lead">UnRO has plenty of staff somone always online.</p>
					<p class="lead">Tons of custom items check our cashshop.</p>
					<p class="lead">Any bugs or balance issues? Please contact us.</p>
					<p> UnRO is running on <a target="_blank" href=" http://rathena.org">rAthena</a>, in our opinoin the best RO emulator to ever do it.</p>
				</section>
			</div>
			<div class="col-sm-6">
				<section class="os-animation" data-os-animation="bounceInRight" data-os-animation-delay=".1s">
					<div class="info-line info-line-vertical clearfix">
					<!-- This is where you put your own contents -->
					<ul>
						<li>
							<div class="entry clearfix">
							<div class="name">No Donations Required</div> 
							We at UnRO feel donations should be given out of want not need. Everything is obtainable in game donate only if you want to help!
							<br />
							</div>
						</li>
						<li>
							<div class="entry clearfix">
							<div class="name">Ragnarok Client</div>  
							One of the best stable clients for Pre. It supports RODex, Achievement, Pet Evolution, M/F Character Creation and many more!
							<br />
							</div>
						</li>
						<li>
							<div class="entry clearfix">
							<div class="name">Server Rates</div> 
							We have a customized rates of 5000x-9000x-1000x (Base-Job-Drop)! MVP Card Drop are at 1000%!
							<br />
							</div>
						</li>
						<li>
							<div class="entry clearfix">
							<div class="name">PvP/WoE Rewards</div>  
							We at UnRO reward PvP/WoE players with great exclusive rewards!
							<br />
							</div>
						</li>
						<li>
							<div class="entry clearfix">
							<div class="name">Cash to VIP Converter</div>  
							Want VIP? Can't donate ATM? No worries we have a converter so you can always have VIP!
							<br />
							</div>
						</li>
						<li>
							<div class="entry clearfix">
							<div class="name">Zeny to Cash Converter</div>  
							Worry not if you want to have cash points without donating! We have a cash-zeny converter! Farm & Grinding is the way!
							<br />
							</div>
						</li>
						<li>
							<div class="entry clearfix">
							<div class="name">Helpful & Event NPC's</div>  
							We have Super Stylist (Free Alternative Costume), Job Changer, Reset Master, Platinum & Universal Rental, Healer, Warper, Private MVP Room, and many more!
							<br />
							</div>
						</li>
						<li>
							<div class="entry clearfix">
							<div class="name">Monthly Cash Shop Sale</div>  
							To reward countless grinding, server host monthly cash shop so that players can buy all items at low-price. Also some exclusive items are being sold! What else?
							<br />
							</div>
						</li>						
					</ul>
					</div>
					<br />
					<center><a href="<?php echo $this->url('main','info'); ?>">Complete server information...</a></center>
				</section>
			</div>
		</div>
	</div>
	</div>

	<div class="padding">
	<div class="container">
		<div class="row">
		
		</div>
	</div>
	</div>
	
	<!-- Additional Column for you to configure -->
	<div class="padding">
	<div class="container">
		<div class="row">
			<div class="col-sm-6">
				<section class="os-animation" data-os-animation="bounceInLeft" data-os-animation-delay=".1s">
					<img src="<?php echo $this->themePath('img/bootstrap2.png'); ?>">
				</section>
			</div>
			<div class="col-sm-6">
				<section class="os-animation" data-os-animation="bounceInRight" data-os-animation-delay=".1s">
					<h2>Bonus for not playing Sura</h2>
					<p>We realize that Sura is a strong class but we dont want 99% of characters to be Sura so we are giving a xp bonus for every class thats not a Sura.</p>
					<p>Don't be alarmed Sura players there has been no change to the class, nor will you be penalized anyway for playing Sura still x5000/x9000, We are just trying to keep server balanced in the beginning.</p>
				</section>
			</div>
		</div>
	</div>
	</div>