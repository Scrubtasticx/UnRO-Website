<?php if (!defined('FLUX_ROOT')) exit; ?>
<?php
	$adminMenuItems = $this->getAdminMenuItems();
	$menuItems = $this->getMenuItems();
	
	if( $params->get('module') != 'main' ): ?>
	<?php if ($session->isLoggedIn()): ?>
	<div id="submenu">
	<?php if (!empty($adminMenuItems) && Flux::config('AdminMenuNewStyle')): ?>
	<?php $mItems = array(); foreach ($adminMenuItems as $menuItem) $mItems[] = sprintf('<a href="%s">%s</a>', $menuItem['url'], Flux::message($menuItem['name'])) ?>
	<b>Admin: </b><?php echo implode(' | ', $mItems) ?>
	<?php endif ?>
	<?php endif ?>
<?php endif ?>
<?php $subMenuItems = $this->getSubMenuItems(); $menus = array() ?>
<?php if (!empty($subMenuItems)): ?>
	<div id="submenu"><b>Menu:</b>
	<?php foreach ($subMenuItems as $menuItem): ?>
		<?php $menus[] = sprintf('<a href="%s" class="sub-menu-item%s">%s</a>',
			$this->url($menuItem['module'], $menuItem['action']),
			$params->get('module') == $menuItem['module'] && $params->get('action') == $menuItem['action'] ? ' current-sub-menu' : '',
			htmlspecialchars($menuItem['name'])) ?>
	<?php endforeach ?>
	<?php echo implode(' | ', $menus) ?>
	</div>
<?php endif ?>