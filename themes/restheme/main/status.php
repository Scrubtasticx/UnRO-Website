<?php
if (!defined('FLUX_ROOT')) exit;

$title = Flux::message('ServerStatusTitle');
$cache = FLUX_DATA_DIR.'/tmp/ServerStatus.cache';
$tbl = Flux::config('FluxTables.OnlinePeakTable'); 


if (file_exists($cache) && (time() - filemtime($cache)) < (Flux::config('ServerStatusCache') * 60)) {
	$serverStatus = unserialize(file_get_contents($cache));
}
else {
	$serverStatus = array();
	foreach (Flux::$loginAthenaGroupRegistry as $groupName => $loginAthenaGroup) {
		if (!array_key_exists($groupName, $serverStatus)) {
			$serverStatus[$groupName] = array();
		}

		$loginServerUp = $loginAthenaGroup->loginServer->isUp();

		foreach ($loginAthenaGroup->athenaServers as $athenaServer) {
			$serverName = $athenaServer->serverName;

			$sql = "SELECT COUNT(char_id) AS players_online FROM {$athenaServer->charMapDatabase}.char WHERE `online` > '0'";
			$sth = $loginAthenaGroup->connection->getStatement($sql);
			$sth->execute();
			$res = $sth->fetch();

			$sqlwoestatus = "SELECT value FROM mapreg WHERE varname = '$\woeStatus' LIMIT 0 , 1";
			$sthwoestatus = $loginAthenaGroup->connection->getStatement($sqlwoestatus);
			$sthwoestatus->execute();
			$woe = $sthwoestatus->fetch();

			if(Flux::config('EnablePeakDisplay')){
				$sth = $server->connection->getStatement("SELECT `users` FROM {$server->charMapDatabase}.$tbl");
				$sth->execute();
				$peak = $sth->fetch();
			}
			$serverStatus[$groupName][$serverName] = array(
				'loginServerUp' => $loginServerUp,
				 'charServerUp' => $athenaServer->charServer->isUp(),
				  'mapServerUp' => $athenaServer->mapServer->isUp(),
				'playersOnline' => intval($res ? $res->players_online : 0),
                  'playersPeak' => intval($peak ? $peak->users : 0),
				'woeStatus' => intval($woe ? $woe->value : 0),
			);
		}
	}
	
	$fp = fopen($cache, 'w');
	if (is_resource($fp)) {
		fwrite($fp, serialize($serverStatus));
		fclose($fp);
	}
}
?>

<?php $i=0;
    $online = "<span style='color:Green'>Online</span>";
    $offline = "<span style='color:Red'>Offline</span>";
    foreach ($serverStatus as $privServerName => $gameServers):
        foreach ($gameServers as $serverName => $gameServer):
            if ($gameServer['loginServerUp']) { $loginserver[$i] = 1; } else { $loginserver[$i] = 0; }
            if ($gameServer['charServerUp']) { $charserver[$i] = 1; } else { $charserver[$i] = 0; }
            if ($gameServer['mapServerUp']) { $mapserver[$i] = 1; } else { $mapserver[$i] = 0; }
            $online_player[$i] = $gameServer['playersOnline'];
			$players_peak[$i] = $gameServer['playersPeak'];
            $woeStatus[$i] = $gameServer['woeStatus'];
            $i++;
        endforeach;
    endforeach;
?>

<?php for ($i=0; $i < count($online_player); $i++): ?>
	<div class="padding">
		<div class="container">
			<div class="row">
				<section class="os-animation" data-os-animation="bounceIn" data-os-animation-delay=".1s">
				<center><h2>Server Status</h2>
					<iframe src="https://freesecure.timeanddate.com/clock/i681sqbk/n145/fn6/fs18/fc444/tt0/tw0/tm1/td2/ts1/tb2" frameborder="0" width="247" height="24"></iframe>
				</center>
				</section>
					<div class="status_margin">
						<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
							<section class="os-animation" data-os-animation="bounceInRight" data-os-animation-delay=".1s">
								<div class="status_main"><?php if ( $mapserver[$i] ) { echo $online; } else { echo $offline; } ?></div>
								<h4><center>Game Server</center></h4>
								<p>Know the status of our game-server! It pays to frequently check to avoid any confusion.</p><p>Online = Server is healthy and running. ROK On! Enjoy!</p><p>Offline = Maintenance or On-going update. ROK Off! Hang on!</p>
							</section>
						</div>
						<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
							<section class="os-animation" data-os-animation="bounceInUp" data-os-animation-delay=".1s">
									<section class="stat" id="stats">
										<div class="status_main">
											<span class="counter text-center"><?php echo $online_player[$i]; ?></span>
										</div>
									</section>
								<h4><center>Online Count</center></h4>
								<p>See how many players are currently online on the server. They can be at town AFK-ing. Instance farming. Player Killing or anything that makes RO fun. ROK On!</p><p>You can always verify it by logging in to check how many are really online</p>
							</section>
						</div>
						<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
							<section class="os-animation" data-os-animation="bounceInDown" data-os-animation-delay=".1s">
									<section class="stat" id="stats">
										<div class="status_main">
											<span class="counter text-center"><?php echo $players_peak[$i]; ?></span>
										</div>
									</section>
								<h4><center>Players Peak</center></h4>
								<p>This tells us how many unique players we have in the server! ROK On!</p>
								<p>We are an international community so expect we have lots of nationalities. Discrimination is not tolerated on the server. We are one family. One love!</p>
							</section>
						</div>
						<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
							<section class="os-animation" data-os-animation="bounceInLeft" data-os-animation-delay=".1s">
								<div class="status_main"><?php if ( $woeStatus[$i] ) { echo $online; } else { echo $offline; } ?></div>
								<h4><center>War of Emperium</center></h4>
								<p>Dominate the castles by conquering and defending them! This will show if the Server's WoE Status is active or not.</p>
								<p>Think you're strong enough to dominate us? Try us! Only the strongest survives! ROK On!</p>
							</section>
						</div>
					</div>
			</div>
		</div>
	</div>
<?php endfor; ?>