<?php if (!defined('FLUX_ROOT')) exit; ?>
					</section>	<!-- Animate -->
				</div> <!-- Column Ending -->
			</div> <!-- Row Ending -->
		</div> <!-- Container Fluid Ending -->
	</div> <!-- Content Padding Ending -->
	<?php if($params->get('module')=='main' AND $params->get('action')=='index'): ?>
	<div id="footer_bg">
	<?php else: ?>
	<?php endif; ?>
	<div class="footer-container">
		<footer class="container-fluid text-center">
			<div class="row">
				<div class="col-sm-4">
					<h3>Join our community!</h3>
					<br>
					<h4>Visit our <a href="forums">Community Boards (Forums)</a> to interact with fellow midgard citizens!</h4>
				</div>
				<div class="col-sm-4">
					<h3>Connect Thru Social Media</h3>
					<a href="FB LINK" target="_blank" class="fa fa-facebook" data-toggle="tooltip" title="Join our FB Page!"></a>
					<a href="FB LINK" target="_blank" class="fa fa-facebook-square" data-toggle="tooltip" title="Join our FB Group!"></a>
					<a href="YT LINK" target="_blank" class="fa fa-youtube" data-toggle="tooltip" title="Subscribe on YT!"></a>
					<a href="DISCORD LINK" target="_blank" class="fa fa-comment" data-toggle="tooltip" title="Communicate thru Discord!"></a>
					<a href="PAYPAL LINK" target="_blank" class="fa fa-paypal" data-toggle="tooltip" title="Donate via Paypal.Me"></a>
				</div>
				<div class="col-sm-4">
					<img src="<?php echo $this->themePath('img/RO_logo.png'); ?>" class="icon" data-toggle="tooltip" title="UnRO">
				</div>
			</div>
		</footer>
	</div>
	<?php if($params->get('module')=='main' AND $params->get('action')=='index'): ?>
  </div>
	<?php else: ?>
	<?php endif; ?>
	</body>
</html>
